# lamp
lamp/do_image: produce a docker image with:
- utf-8
- noninteractive locale configuration
- latest LTS ubuntu
- apache2
- php (options: 5.6 or 7.2)
- mariaDB (with password given)
- adminer on /adminer.php

# certificats
- cerfificats/certif1: produces letsencrypt certificates where the apache of the host is the proxy
- certificats/certif2: produces letsencrypt certificates where a dockersite is the proxy 

# virtualhosts
- virtualhosts/dockersite80	    produces a docker container with an apache to manage a domain name in http
- virtualhosts/dockersite443	produces a docker container with an apache to manage a domain name in https
- virtualhosts/vhapache80		declares a docker to the apache proxy in http
- virtualhosts/vhapache443      declares a docker to the apache proxy in https	
- virtualhosts/vhproxy80		declares a docker to a docker proxy in http
- virtualhosts/vhproxy443		declares a docker to a docker proxy in https

