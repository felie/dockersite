# Usage:
./do_image <image> <php version (5.6 ou 7.2)> <root password for mysql>

Make a docker image automatically (without any intervention)

- ubuntu (latest LTS)
  - utf-8
  - time zone
  - apache2
    - php5.6 ou 7.2 (choose!)
  - mariadb5.5
    - adminer on /adminer.php
 
# Mecanism
./do_image <nom de l'image> <version de php> <mot de passe root de mysql>
- make a container from ubuntu
- copy lamp_build_image inside
- execute it inside
- commit the image from this container

Note: locale and time zone have to be change in the lamp_build_image script (Europe/Paris and fr_FR.UTF-8)
