# certif1
certif1 script allows to obtain automatically a letsencrypt ssl certificate  in the case of the host's apache is the proxy

parameters
- a mail
- complete domain 
- mode (test or real) - the test mode is --staging of letsencrypt

# certif2
certif1 script allows to obtain automatically a letsencrypt ssl certificate  in the case of the proxy is a dockersite

parameters
- a mail
- complete domain 
- mode (test or real) - the test mode is --staging of letsencrypt
- the name of the proxy dockersite

certif2 have to 
- stop the proxy
- start host's apache (for the letsencrypt challenge)
- obtain certiticate
- stop host's apache
- restart the proxy

